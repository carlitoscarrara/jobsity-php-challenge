<div class="tz-blogItem">
    <div class="tz-blogContent">
        <div class="tz-blogBox ">
            <div class="tz-BlogImage">
                <img src="{{$entry->img_path}}" alt="blog-quote">
            </div>
            <div class="tz_blog_box_content">
                <h4 class="title">
                    <a href="javascript:void(0)"> {{$entry->title}} </a>
                </h4>
                <span class="tzinfomation">
                    <small class="tzinfomation_time"> {{date('l jS \of F Y', strtotime($entry->created_at)) }} </small>
                    <a href="{{ route('user.show', $entry->user->id) }}">
                        <i>|</i>
                        by {{$entry->user->name}}
                    </a>
                </span>

                <p>
                    {{$entry->content}}
                </p>

                @if( !Auth::guest() )
                    @if ($entry->user->id === Auth::user()->id)
                        <a class="tzreadmore" href="{{ route('entry.edit', $entry->id) }}">
                            <span>EDIT ENTRY</span>
                        </a>
                    @endif
                @endif

            </div>
        </div>
    </div>
</div>