@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="tz_meetup_wpcf7-form">
    <p>
        <span class="wpcf7-form-control-wrap">
            {!! Form::text('title', null, ['class' => 'wpcf7-form-control wpcf7-text', 'placeholder' => 'TITLE *', 'size' => '40', 'name' => 'title']) !!}
        </span>
        <i class="fa fa-quote-right"></i>
    </p>
    <p>
        <span class="wpcf7-form-control-wrap email-78">
            {!! Form::text('img_path', null, ['class' => 'wpcf7-form-control wpcf7-text', 'placeholder' => 'IMAGE URL', 'size' => '40', 'name' => 'img_path']) !!}
        </span>
        <i class="fa fa-image"></i>
    </p>
    <p class="tz_meetup_contact_textarea">
        <span class="wpcf7-form-control-wrap">
            {!! Form::textarea('content', null, ['class' => 'wpcf7-form-control wpcf7-textarea', 'placeholder' => 'CONTENT *', 'rows' => '10', 'name' => 'content']) !!}
        </span>
        <i class="fa fa-align-left"></i>
    </p>
    <p>
        <input class="wpcf7-form-control wpcf7-submit" type="submit" value="Submit Entry">
    </p>
</div>

@csrf