<div class="tz-blogContainer">

    @if (count($entries) === 0)
        <h2>No entries found :|</h2>
    @endif

    {{--Display all entries--}}
    @foreach($entries as $entry)

        @component('components.entry')
            @slot('entry', $entry)
        @endcomponent

    @endforeach

</div>

{{--Pagination nav--}}
@if (count($entries) > 0)
<div class="wp-pagenavi">
    @if (!$entries->onFirstPage())
        <a class="nextpostslink" href="{{ $entries->previousPageUrl() }} " rel="prev">«</a>
    @endif
    <span class="pages">Page {{ $entries->currentPage() }} of {{ $entries->lastPage() }}</span>
    @if ($entries->currentPage() !== $entries->lastPage())
        <a class="nextpostslink" href="{{ $entries->nextPageUrl() }} " rel="next">»</a>
    @endif
</div>
@endif