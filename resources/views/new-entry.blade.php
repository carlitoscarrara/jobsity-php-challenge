@extends('layouts.app')

@section('title')
    <i class="fa fa-align-justify"></i> New Entry
@endsection

@section('content')
    <section id="tz_contact">

        <div class="tz_contact_icon_form">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">

                        <h2>Create your new blog entry:</h2>

                        {!! Form::open(['route' => 'entry.store', 'method' => 'post', 'class' => 'wpcf7-form']) !!}
                            @component('components.entry-form')
                            @endcomponent
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection