<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Jobsity Challenge</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic' rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/prettyPhoto.min.css') }}" rel="stylesheet">

    <!-- Style css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=" {{ asset('js/library/html5shiv.min.js') }} "></script>
    <script src=" {{ asset('js/library/respond.min.js') }} "></script>
    <![endif]-->
</head>
<body id="bd">
<div id="tzwrapper">

    <!-- Header start -->
    <header class="tz-headerHome tz-homeType2 tz-homeTypeRelative">
        <div class="tz-header-content">
            <div class="container">
                <div class="tzHeaderContainer">
                    <h3 class="pull-left tz_logo">
                        <a title="Home" href="/">
                            <img src="{{ asset('images/jobsity-logo.png') }}">
                        </a>
                    </h3>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tz-navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Menu start -->
                    <nav class="nav-collapse pull-right tz-menu">
                        <ul id="tz-navbar-collapse" class="nav navbar-nav collapse navbar-collapse tz-nav">

                            @if( Auth::user() )
                                <li><a href="{{ route('index') }}">Home</a></li>

                                <li><a href="{{ route('entry.create') }}">New Entry</a></li>

                                <li class="menu-item-has-children">
                                    <a href="{{ route('user.show', Auth::user()->id) }}"> <i class="fa fa-user"></i> {{ Auth::user()->name }} </a>
                                    <ul class="sub-menu non_mega_menu">
                                        <li class="menu-item">
                                            <a href="{{ route('user.show', Auth::user()->id) }}">Profile</a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="{{ route('logout') }}">Logout</a>
                                        </li>
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @endif
                        </ul>
                    </nav>
                    <!-- Menu end -->

                </div>
            </div>
        </div>
    </header>
    <!-- Header end -->

    <!-- Title start -->
    <section class="tz-sectionBreadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="tz_breadcrumb_single_cat_title">
                        <h4> @yield('title') </h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Title end -->

    @yield('content')

    <!-- footer start -->
    <footer class="tz-footer tz-footer-type1">
        <div class="tz-backtotop">
            <img src="{{ asset('images/back_top_meetup.png') }}" alt="back_top">
        </div>
        <aside class="MultiColorSubscribeWidget widget">
            <h3 class="module-title">
                <span>have a great day..!</span>
            </h3>
        </aside>
        <div class="tzcopyright">
            <p>
                Carlos Carrara - carlitos.carrara@hotmail.com
            </p>
        </div>
    </footer>
    <!-- footer end -->

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=" {{ asset('js/library/jquery.min.js') }} "></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src=" {{ asset('js/library/bootstrap.min.js') }} "></script>
<script>
    jQuery.noConflict();
</script>
<!-- Include bxslider -->
<script src=" {{ asset('js/library/jquery.prettyPhoto.js') }} "></script>

<!-- Include custom js -->
<script src=" {{ asset('js/custom.js') }} "></script>

@yield('scripts')

</body>
</html>