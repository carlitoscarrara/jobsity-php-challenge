@extends('layouts.app')

@section('title')
    <i class="fa fa-home"></i> Home
@endsection

@section('content')
    <!-- Entries start -->
    <section class="tz-blogDefault">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @component('components.entries')
                    @slot('entries', $entries)
                @endcomponent
            </div>
        </div>
    </section>
    <!-- Entries end -->
@endsection