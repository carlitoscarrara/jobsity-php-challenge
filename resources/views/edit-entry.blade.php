@extends('layouts.app')

@section('title')
    <i class="fa fa-pencil"></i> Edit Entry
@endsection

@section('content')
    <section id="tz_contact">

        <div class="tz_contact_icon_form">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">

                        <h2>Edit your entry:</h2>

                        {!! Form::model($entry, ['route' => ['entry.update', $entry->id], 'method' => 'put', 'class' => 'wpcf7-form']) !!}
                            @component('components.entry-form')
                            @endcomponent
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection