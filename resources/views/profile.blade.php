@extends('layouts.app')

@section('title')
    <i class="fa fa-user"></i> {{ $user->name }} | Entries
@endsection

@section('content')
    <!-- Entries start -->
    <section class="tz-blogDefault">
        <div class="container">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                @component('components.entries')
                    @slot('entries', $entries)
                @endcomponent
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="tz-sidebar">

                    <aside id="dw_twitter-2" class="dw_twitter latest-twitter widget">
                        <h3 class="module-title">
                            <span>Twitter</span>
                        </h3>
                        <div class="dw-twitter-inner" id="tweets-container">

                            <span style="font-size: 3rem;"><i class="fa fa-spinner fa-spin"></i></span>

                        </div>

                    </aside>

                    @if ($isUserProfile)

                    <aside id="dw_twitter-2" class="dw_twitter latest-twitter widget">
                        <h3 class="module-title">
                            <span>Hidden Tweets</span>
                        </h3>
                        <div class="dw-twitter-inner" id="hidden-tweets-container">

                            <span style="font-size: 3rem;"><i class="fa fa-spinner fa-spin"></i></span>

                        </div>

                    </aside>

                    @endif

                </div>
            </div>

        </div>
    </section>
    <!-- Entries end -->
@endsection

@section('scripts')
    <script>
        jQuery(document).ready(function () {
            displayUserTweets();

            displayUserHiddenTweets();
        });

        function displayUserTweets () {
            jQuery("#tweets-container").html("<span style=\"font-size: 3rem;\"><i class=\"fa fa-spinner fa-spin\"></i></span>");

            //Get User Hidden Tweets
            jQuery.get("/getUserTweets/{{ $user->id }}", function (tweets) {
                let tweetsHTML = '';

                if (tweets === undefined || tweets.length == 0) {
                    tweetsHTML = tweetsHTML.concat(
                        "<h5> No tweets to show for {{ '@' . $user->twitter_username }} :( </h5>"
                    );
                } else {
                    //Iterate an add tweet to the list
                    Object.keys(tweets).forEach(function(key){
                        let tweet = tweets[key];

                        let tweetId  = tweet.id_str;
                        let tweetText  = tweet.text;
                        let tweetUserId  = tweet.user.id;
                        let tweetDate  = new Date(tweet.created_at).toLocaleDateString('en-ES', { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' });

                        tweetsHTML = tweetsHTML.concat(
                            "<div class=\"tweet-item user_timeline\">\n" +
                            "    <div class=\"tweet-content\">\n" +
                                    tweetText +
                            "        <span class=\"time\">\n" +
                            "        <a href=\'http://twitter.com/" + tweetUserId + "/status/" + tweetId + "' target='_blank'>" + tweetDate + "</a>\n" +
                            "        </span>\n" +
                            @if($isUserProfile)
                                      "<a href='javascript:hideTweet(\"" + tweetId + "\")'><i class='fa fa-eye-slash' data-toggle='tooltip' title='Hide Tweet'></i> Hide Tweet</a>\n" +
                            @endif

                            "    </div>\n" +
                            "</div>");
                    });
                }

                jQuery("#tweets-container").html(tweetsHTML);
            });
        }

        @if ($isUserProfile)
        function displayUserHiddenTweets () {
            jQuery("#hidden-tweets-container").html("<span style=\"font-size: 3rem;\"><i class=\"fa fa-spinner fa-spin\"></i></span>");

            //Get User Hidden Tweets
            jQuery.get("/getUserHiddenTweets", function (tweets) {
                let tweetsHTML = '';

                if (tweets === undefined || tweets.length == 0) {
                    tweetsHTML = tweetsHTML.concat(
                        "<h5> No hidden tweets </h5>"
                    );
                } else {
                    //Iterate an add tweet to the list
                    Object.keys(tweets).forEach(function(key){
                        let tweet = tweets[key];

                        let tweetId  = tweet.id_str;
                        let tweetText  = tweet.text;
                        let tweetUserId  = tweet.user.id;
                        let tweetDate  = new Date(tweet.created_at).toLocaleDateString('en-ES', { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' });

                        tweetsHTML = tweetsHTML.concat(
                            "<div class=\"tweet-item user_timeline\">\n" +
                            "    <div class=\"tweet-content\">\n" +
                            tweetText +
                            "        <span class=\"time\">\n" +
                            "        <a href=\'http://twitter.com/" + tweetUserId + "/status/" + tweetId + "' target='_blank'>" + tweetDate + "</a>\n" +
                            "        </span>\n" +
                                    "<a href='javascript:showTweet(\"" + tweetId + "\")'><i class='fa fa-eye' data-toggle='tooltip' title='Show Tweet'></i> Show Tweet</a>\n" +
                            "    </div>\n" +
                            "</div>");
                    });
                }

                jQuery("#hidden-tweets-container").html(tweetsHTML);
            });
        }
        @endif

        function hideTweet (tweetId) {
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/hideTweet/" + tweetId,
                success: function () {
                    //Refresh tweets
                    displayUserTweets();
                    displayUserHiddenTweets();
                },
            });
        }

        function showTweet (tweetId) {
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/showTweet/" + tweetId,
                success: function () {
                    //Refresh tweets
                    displayUserTweets();
                    displayUserHiddenTweets();
                },
            });
        }
    </script>
@endsection