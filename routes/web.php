<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::resource('/', 'EntryController');
Route::resource('entry', 'EntryController');

Route::resource('user', 'UserController');

Route::post('hideTweet/{tweet_id}', 'TweetsController@hideTweet');
Route::post('showTweet/{tweet_id}', 'TweetsController@showTweet');
Route::get('getUserTweets/{user_id}', 'TweetsController@getUserTweets');
Route::get('getUserHiddenTweets', 'TweetsController@getUserHiddenTweets');
