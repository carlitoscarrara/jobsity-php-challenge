<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HiddenTweet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'tweet_id',
    ];

    /**
     *  Relationships
     */

    /**
     * Get the user of a hidden tweet.
     */

    public function user() {
        return $this->belongsTo('App\HiddenTweet');
    }
}
