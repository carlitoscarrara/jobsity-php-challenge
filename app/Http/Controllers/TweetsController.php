<?php

namespace App\Http\Controllers;

use App\User;
use App\HiddenTweet;

use Illuminate\Http\Request;
use Abraham\TwitterOAuth\TwitterOAuth;

use Config;
use Auth;

class TweetsController extends Controller
{
    private $twConnection;

    //Constructor
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'getUserTweets',
        ]]);

        //Initialize Twitter API Connection
        //Twitter API Credentials
        $consumer_api_key = Config::get('twitter.consumer_api_key');
        $consumer_api_secret_key = Config::get('twitter.consumer_api_secret_key');
        $access_token = Config::get('twitter.access_token');
        $access_token_secret = Config::get('twitter.access_token_secret');

        //Twitter API Connection
        $connection = new TwitterOAuth($consumer_api_key, $consumer_api_secret_key, $access_token, $access_token_secret);

        $this->twConnection = $connection;
    }

    //Stores the tweet with that id on the database as a hidden tweet
    public function hideTweet($tweet_id) {
        $user = Auth::user();

        $tweet = $this->twConnection->get("statuses/show", ["id" => $tweet_id]);

        //Validate that a user is hiding a tweet he owns
        if (strtolower($tweet->user->screen_name) === strtolower($user->twitter_username)) {
            //Store hidden tweet
            $hiddenTweet = new HiddenTweet();
            $hiddenTweet->user_id = $user->id;
            $hiddenTweet->tweet_id = $tweet_id;
            $hiddenTweet->save();
        }
    }

    //Removes the tweet with that id on the database as a hidden tweet
    public function showTweet($tweet_id) {
        $user = Auth::user();

        $hiddenTweet = HiddenTweet::where('tweet_id', $tweet_id)->first();

        //Validate if the user owns the hidden tweet
        if ($hiddenTweet->user_id == $user->id) {
            $hiddenTweet->delete();
        }
    }

    //Retreive the latest non hidden tweets of a user by its user id.
    public function getUserTweets($user_id) {

        //Latest 5 tweets request
        $user = User::find($user_id);
        $twitter_username = $user->twitter_username;
        $tweets = $this->twConnection->get("statuses/user_timeline", ["screen_name" => $twitter_username, "count" => 5, "exclude_replies" => true]);

        //If there are no tweets responded
        if (array_key_exists('error', $tweets) || array_key_exists('errors', $tweets)) {
            $tweets = null;
        } else {
            //Remove the hidden tweets
            $hiddenTweets = HiddenTweet::where('user_id', $user_id)->get();
            $i = 0;
            foreach ($tweets as $tweet) {
                $isAHiddenTweet = in_array($tweet->id, $hiddenTweets->pluck('tweet_id')->toArray());

                if ($isAHiddenTweet) {
                    unset($tweets[$i]);
                    //array_splice($tweets, $i, 1);
                }

                $i++;
            }
        }

        return $tweets;
    }

    //Retreive all the hidden tweets of the logged user.
    public function getUserHiddenTweets() {
        $user = Auth::user();

        $hiddenTweets = HiddenTweet::where('user_id', $user->id)->get();

        $hiddenTweetsFromTw = [];
        foreach ($hiddenTweets as $hiddenTweet) {
            $hiddenTweetFromTw = $this->twConnection->get("statuses/show", ["id" => $hiddenTweet->tweet_id]);

            array_push($hiddenTweetsFromTw, $hiddenTweetFromTw);
        }

        return $hiddenTweetsFromTw;
    }
}
