<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class CustomInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:init {--seed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear config cache, refresh migrations, run seeders (optional) and install passport.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Clearing cache...');
        $exitCode = Artisan::call('cache:clear');
        $this->comment($exitCode);

        $this->info('Clearing configurations...');
        $exitCode = Artisan::call('config:clear');
        $this->comment($exitCode);

        if ($this->option('seed')) {
            $this->info('Refreshing migrations and seeding database...');
            $exitCode = Artisan::call('migrate:refresh --seed');
            $this->comment($exitCode);
        } else {
            $this->info('Refreshing migrations without seeding database...');
            $exitCode = Artisan::call('migrate:refresh');
            $this->comment($exitCode);
        }

        $this->info('Done! Ready 2 go.');
    }
}
