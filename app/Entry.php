<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'img_path'
    ];

    /**
     *  Relationships
     */

    /**
     * Get the user of the entry.
     */

    public function user() {
        return $this->belongsTo('App\User');
    }
}
