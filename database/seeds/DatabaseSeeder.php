<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Create 30 users (using UserFactory)
        $users = factory(App\User::class, 30)->create();

        //Create 100 entries (using EntryFactory)
        $entries = factory(App\Entry::class, 100)->create();
    }
}
