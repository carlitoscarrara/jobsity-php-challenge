<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entry;
use Faker\Generator as Faker;

$factory->define(Entry::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'content' => $faker->text,
        'img_path' => 'https://source.unsplash.com/' . $faker->numberBetween(1000, 1200) . 'x' . $faker->numberBetween(250, 300),

        'user_id' => $faker->numberBetween(1, 30),
    ];
});
