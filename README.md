# Jobsity PHP Challenge

### External Dependecies
*  Laravel Collective - https://laravelcollective.com/
*  TwitterOAuth - https://github.com/abraham/twitteroauth

### Setup

Download the project and run the following commands:
1.  composer install
2.  php artisan key:generate
3.  php artisan custom:init --seed
(In case you do not want fake data, ignore the *--seed* option.)





